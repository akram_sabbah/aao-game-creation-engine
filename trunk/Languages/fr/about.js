{
	"about_aao" : "À propos d'Ace Attorney Online",
	
	"aao_core_team" : "L'équipe dirigeante",
	
	"aao_role_unas" : "Fondateur, Développeur, Administrateur",
	"aao_role_meph" : "Responsable de la communauté anglaise, Designer",
	"aao_role_kroki" : "Responsable de la communauté française",
	"aao_role_daniel" : "Responsable de la communauté espagnole",
	"aao_role_thepasch" : "Responsable de la communauté allemande",
	
	"aao_supporting_teams" : "Les équipes auxiliaires",
	
	"aao_mods" : "Modérateurs du forum",
	"aao_qa_en" : "Évaluateurs anglais",
	"aao_qa_fr" : "Évaluateurs français",
	"aao_qa_es" : "Évaluateurs espagnols",
	"aao_qa_de" : "Évaluateurs allemands",
	
	"aao_special_thanks" : "Remerciements",
	
	"aao_role_spparrow" : "Fondateur, Idée originelle",
	"aao_role_devsupport" : "Aide au développement",
	"aao_role_translation" : "Aide à la traduction",
	"aao_role_music" : "Compositeurs de musiques originales"
}
