{
	"date_format" : "d/m/Y",
	"time_format" : "H:i",
	
	"home" : "Home",
	"search" : "Games",
	"manager" : "Manager",
	"forums" : "Forums",
	"help" : "Help",
	"about" : "About",
	
	"copyright_aao" : "Ace Attorney Online © Unas & Spparrow 2006–<year>. It is forbidden to copy content from this website without the webmaster's consent.",
	"copyright_capcom" : "Ace Attorney® and associated properties are © Capcom, Ltd. Ace Attorney Online is not associated with Capcom and is run solely by fans.",
	"disclaimer_title" : "Disclaimer",
	"disclaimer_text" : "This website enables you to highlight your creativity. It is not a way to avoid buying Ace Attorney games.",
	
	"profile_judge" : "Judge"
}
