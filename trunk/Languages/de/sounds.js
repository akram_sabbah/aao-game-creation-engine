{
"default_sounds_Avale" : "Sprachemotionen|Gulp",
"default_sounds_Awe" : "Sprachemotionen|Oh nein!",
"default_sounds_Baffe" : "Sprachemotionen|Ohrfeige",
"default_sounds_Bourde" : "",
"default_sounds_Bwaaah" : "Gericht|Strafe",
"default_sounds_ClacManfred" : "Sprachemotionen|Fingerschnipsen",
"default_sounds_Coup" : "Gericht|Game Over",
"default_sounds_Coupdefeu" : "Soundeffekte|Lauter Schuss",
"default_sounds_Coupdefeu2" : "Soundeffekte|Leiser Schuss",
"default_sounds_Ding" : "Sprachemotionen|Idee",
"default_sounds_Dunnn" : "Sprachemotionen|Schockiert",
"default_sounds_Estomac qui gargouille" : "Sprachemotionen|Magenknurren",
"default_sounds_Fouet" : "Sprachemotionen|Wut",
"default_sounds_Fouet2" : "Charakterspezifisch|Franziska - Peitsche",
"default_sounds_Gotcha Apollo" : "Sprache|Apollo - Ertappt! (ENG)",
"default_sounds_Guilty" : "Gericht|Schuldig",
"default_sounds_Guitare" : "Charakterspezifisch|Kantilen - Luftgitarre",
"default_sounds_Harmonica" : "Charakterspezifisch|Eldoon - Harmonika",
"default_sounds_Hold It Apollo" : "Sprache|Apollo - Moment mal!",
"default_sounds_Hold it Edgeworth" : "Sprache|Edgeworth - Moment mal! (ENG)",
"default_sounds_Hold it Mia" : "Sprache|Mia - Moment mal! (ENG)",
"default_sounds_Hold it Phoenix" : "Sprache|Phoenix - Moment mal! (ENG)",
"default_sounds_Hush" : "Soundeffekte|Flashback",
"default_sounds_Key" : "Sprachemotionen|Huh?",
"default_sounds_Marteau" : "Gericht|Hammer - ×1",
"default_sounds_Notguilty" : "Gericht|Nicht schuldig", 
"default_sounds_Objection Apollo" : "Sprache|Apollo - Einspruch! (ENG)",
"default_sounds_Objection Boulay" : "Sprache|Payne - Einspruch! (ENG)",
"default_sounds_Objection Edgeworth" : "Sprache|Edgeworth - Einspruch! (ENG)",
"default_sounds_Objection Franziska" : "Sprache|Franziska - Einspruch! (ENG)",
"default_sounds_Objection Godot" : "Sprache|Godot - Einspruch! (ENG)",
"default_sounds_Objection Konrad" : "Sprache|Klavier - Einspruch! (ENG)",
"default_sounds_Objection Kristoph" : "Sprache|Kristoph - Einspruch! (ENG)",
"default_sounds_Objection Manfred" : "Sprache|Manfred - Einspruch! (ENG)",
"default_sounds_Objection Mia" : "Sprache|Mia - Einspruch! (ENG)",
"default_sounds_Objection Phoenix" : "Sprache|Phoenix - Einspruch! (ENG)",
"default_sounds_OuverturePorte" : "Soundeffekte|Tür öffnen",
"default_sounds_Photo" : "Soundeffekte|Kamera",
"default_sounds_Pistolet Extraterrestre" : "Charakterspezifisch|Oldbag - Laserpistole",
"default_sounds_Rolling" : "Psychische Blockaden|Psychische Blockaden erscheinen",
"default_sounds_Rumeur" : "Gericht|Gerüchte im Gerichtssaal",
"default_sounds_SelectJingle" : "Soundeffekte|Neuer Beweis",
"default_sounds_Shatter" : "Psychische Blockaden|Blockade bricht",
"default_sounds_Shing" : "Sprachemotionen|Schreien",
"default_sounds_Shock" : "Sprachemotionen|Waaah!",
"default_sounds_Slash" : "Sprachemotionen|Owned",
"default_sounds_TableSlam" : "Soundeffekte|Desk Slam",
"default_sounds_Take that Apollo" : "Sprache|Apollo - Nimm das! (ENG)",
"default_sounds_Take that Edgeworth" : "Sprache|Edgeworth - Nimm das! (ENG)",
"default_sounds_Take that Mia" : "Sprache|Mia - Nimm das! (ENG)",
"default_sounds_Take that Phoenix" : "Sprache|Phoenix - Nimm das! (ENG)",
"default_sounds_Tri-Gavel" : "Gericht|Hammer - ×3",
"default_sounds_Unvealed" : "Gericht|Aussage 2",
"default_sounds_VICTORY" : "Gericht|Sieg!",
"default_sounds_Whaaa" : "Gericht|Aussage 1",
"default_sounds_Whoops" : "Sprachemotionen|Whoops"
}
