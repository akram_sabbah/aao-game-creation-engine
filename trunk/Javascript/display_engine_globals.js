/*
Ace Attorney Online - Global functions used by several modules of the display engine

*/

//MODULE DESCRIPTOR
Modules.load(new Object({
	name : 'display_engine_globals',
	dependencies : ['style_loader', 'events', 'objects'],
	init : function() {
		includeStyle('top_screen_engine');
	}
}));

//INDEPENDENT INSTRUCTIONS

//EXPORTED VARIABLES

//EXPORTED FUNCTIONS

/*
The callback buffer allows buffering several callbacks in order to run them all at the same time, once the source events have all been shot.

The method "trigger" adds a callback function waiting for a notification of completion.
It takes as argument the callback function to run in the end, and returns the "fake" callback function to use to notify of the completion of the source event.
Typical use : img.addEventListener('load', buffer.trigger(function() { ... }), false);

The method "call" adds a callback function that is not waiting only for the completion of other source events.
It takes as argument the callback function to run, and does not return anything.
*/
function callbackBuffer() {
	this.callbacks = [];
	this.numberOfPendingTriggers = 0;
	this.pendingTimeout = 0;
	
	this.checkAndrun = (function() {
		if(this.numberOfPendingTriggers <= 0)
		{
			var callback;
			while(callback = this.callbacks.shift())
			{
				callback();
			}
			
			this.numberOfPendingTriggers = 0;
			if(this.pendingTimeout)
			{
				window.clearTimeout(this.pendingTimeout);
				this.pendingTimeout = 0;
			}
		}
	}).bind(this);
	
	this.resolvePendingTrigger = (function() {
		this.numberOfPendingTriggers--;
		this.checkAndrun();
	}).bind(this);
	
	this.trigger = function(callback) {
		this.callbacks.push(callback);
		this.numberOfPendingTriggers++;
		
		// If there is at least one trigger, the timeout is useless - execution will only take place after all triggers.
		if(this.pendingTimeout)
		{
			window.clearTimeout(this.pendingTimeout);
			this.pendingTimeout = 0;
		}
		
		return this.resolvePendingTrigger;
	};
	
	this.call = function(callback) {
		this.callbacks.push(callback);
		
		// If there is no pending trigger, do not call immediately (leave time to buffer other tasks in the same execution flow)
		// but make sure that this will be called at the end of the execution flow.
		if(!this.pendingTimeout && this.numberOfPendingTriggers <= 0)
		{
			this.pendingTimeout = window.setTimeout(this.checkAndrun, 0);
		}
	};
	
	return this;
}

// Generate a javascript Image element, making sure the animation restarts if any, and callback
function generateImageElement(uri, callback, callback_buffer)
{
	function handleLoadError(image, loadCompleteTrigger)
	{
		console.log(image.src, 'failed to load');
		//debugger;
		
		// Force dimensions to be non-null, since place rendering relies on bg dimensions
		image.width = 256;
		image.height = 192;
		
		if(loadCompleteTrigger)
		{
			loadCompleteTrigger();
		}
	}
	
	var img = new Image();
	
	var loadCompleteTrigger;
	if(callback_buffer && callback)
	{
		loadCompleteTrigger = callback_buffer.trigger(callback.bind(undefined, img));
	}
	else if(callback)
	{
		loadCompleteTrigger = callback.bind(undefined, img);
	}
	else
	{
		loadCompleteTrigger = null;
	}
	
	if(uri == '')
	{
		// Avoid launching a request to the current page, or being stuck by https://bugzilla.mozilla.org/show_bug.cgi?id=599975
		
		// Defer load error handling : callback should not be called during the execution of the current flow.
		window.setTimeout(handleLoadError.bind(undefined, img, loadCompleteTrigger), 0);
		return img;
	}
	
	registerEventHandler(img, 'load', function()
	{
		// Reset animation : Webkit seems to restart only when resetting an object, not when creating a new one...
		unregisterEvent(img, 'load'); // Avoid loop of load events...
		window.setTimeout(function()
		{
			img.src = uri; // Reassign uri after a null timeout : webkit sometimes doesn't react if it's done inline
			
			if(loadCompleteTrigger)
			{
				loadCompleteTrigger();
			}
		}, 0);
	}, false);
	registerEventHandler(img, 'error', handleLoadError.bind(undefined, img, loadCompleteTrigger), false);
	img.src = uri;
	
	return img;
}

// Generate a graphic element from its descriptor
function generateGraphicElement(graphic_descriptor, callback, callback_buffer)
{
	if('uri' in graphic_descriptor)
	{
		//if this describes an image file, load it
		var element = document.createElement('div');
		
		var inner_element = document.createElement('div');
		addClass(inner_element, 'inner_elt');
		element.appendChild(inner_element);
		
		var uri = graphic_descriptor.uri;
		
		var img = generateImageElement(uri, function(img)
		{
			// Set parent element's size
			element.style.height = img.height + 'px';
			element.style.width = img.width + 'px';
			inner_element.style.height = img.height + 'px';
			inner_element.style.width = img.width + 'px';
			
			if(callback)
			{
				callback(img, element);
			}
		}, callback_buffer);
		inner_element.appendChild(img);
		
		return element;
	}
	else
	{
		// TODO : handle non-picture elements some day ?
		debugger;
	}
};

// Update a graphic element using a descriptor
function updateGraphicElement(element, graphic_descriptor, callback, callback_buffer)
{
	var inner_element = element.firstChild;
	
	if('uri' in graphic_descriptor)
	{
		var new_img = generateImageElement(graphic_descriptor.uri, function(img)
		{
			// Set parent element's size
			element.style.height = img.height + 'px';
			element.style.width = img.width + 'px';
			inner_element.style.height = img.height + 'px';
			inner_element.style.width = img.width + 'px';
			
			inner_element.replaceChild(img, inner_element.firstChild); // Replace the image.
			
			if(callback)
			{
				callback(img, element);
			}
		}, callback_buffer);
	}
	else
	{
		// TODO : handle non-picture elements some day ?
		debugger;
	}
}

function setEffectToGraphicElement(element, effect_name, enabled)
{
	var inner_element = element.firstChild;
	var img = inner_element.lastChild;
	
	switch(effect_name)
	{
		case 'mirror':
			
			if(enabled)
			{
				addClass(inner_element, 'mirrored');
			}
			else
			{
				removeClass(inner_element, 'mirrored');
			}
			
			break;
		
		default:
			debugger;
			break;
	}
}

// Set the position of a graphic element on the scene
function setGraphicElementPosition(element, position, container, transition, callback)
{
	var inner_element = element.firstChild;
	
	var transition_duration = transition ? 0.5 : 0;
	setTransition(element, transition_duration, true, 'left');
	setTransition(inner_element, transition_duration, true, 'left');
	
	inner_element.style.left = position.shift+'px';
	switch(position.align)
	{
		case ALIGN_LEFT :
			element.style.left = 0;
			break;
		
		case ALIGN_CENTER :
			element.style.left = Math.floor((container.clientWidth / 2) - (element.clientWidth / 2))+'px';
			break;
		
		case ALIGN_RIGHT : 
			element.style.left = Math.floor(container.clientWidth - element.clientWidth)+'px';
			break;
	}
	
	// Vertically align on the bottom
	element.style.bottom = 0;
	
	// TODO : really handle vertical align as well some day ?
	
	// Callback when move is complete.
	if(callback)
	{
		window.setTimeout(callback, transition_duration * 1000);
	}
};

function setTransition(node, duration, only_once, property)
{
	if(!node)
	{
		//if node has been deleted while transitioning, do not crash
		return;
	}
	
	//duration should only be 0 (disabled) or 0.5 (enabled). Any other value will result in 0.5
	if(duration)
	{
		//set the transition end time
		var duration_ms = duration * 1000;
		var transition_start = (new Date()).getTime();
		var transition_end = transition_start + duration_ms;
		if(!node.transition_end || node.transition_end < transition_end)
		{
			node.transition_end = transition_end;
		}
		
		//apply transition, possibly limiting to a property
		addClass(node, 'transitioning' + (property ? '-' + property : ''));
		
		if(only_once) //if transition is to be ran only once, remove it after a while
		{
			window.setTimeout(function(){
				//cancel transition only if there isn't another transition scheduled to end later
				if(node.transition_end == transition_end)
				{
					setTransition(node, 0, false, property);
				}
			}, duration_ms);
		}
	}
	else
	{
		removeClass(node, 'transitioning' + (property ? '-' + property : ''));
	}
}

//END OF MODULE
Modules.complete('display_engine_globals');
